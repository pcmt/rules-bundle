# PCMT Rules Bundle for Akeneo Pim

## Key features

Rules Bundle provides a new kind of jobs which allow to simply copy data between products and product models.

## Installation

To install the bundle, you need to manually add some rows in composer.json file.

```
// ...
"repositories": [

    // ...
    
    {
        "type": "vcs",
        "url": "https://gitlab.com/pcmt/rules-bundle.git",
        "branch": "master",
        "no-api": true
    },
    
    // ...
    
],
"require": {

    // ...
    
    "pcmt/rules-bundle": "dev-master@dev",
    
    // ...
    
},

//...
```

After that simply run `composer update` command.

### Enable the bundle
Enable the bundle in the kernel:

```php
<?php
// config/bundles.php

return [
    // ...
    // PcmtSharedBundle is required, because it contains some abstraction which are used in PcmtRulesBundle
    PcmtSharedBundle\PcmtSharedBundle::class => ['all' => true], 
    PcmtRulesBundle\PcmtRulesBundle::class => ['all' => true],
];
```

## Development
### Running Test-Suits
The PcmtRulesBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtRulesBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
