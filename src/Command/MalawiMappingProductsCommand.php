<?php
/*
 * Copyright (c) 2024, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtRulesBundle\Command;

use PcmtRulesBundle\Malawi\RuleProcessStep;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MalawiMappingProductsCommand extends Command
{
    /**
     * @var RuleProcessStep
     */
    private $ruleProcessStep;

    public function __construct(RuleProcessStep $ruleProcessStep)
    {
        $this->ruleProcessStep = $ruleProcessStep;

        parent::__construct();
    }

    public function configure(): void
    {
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->ruleProcessStep->execute();

        return Command::SUCCESS;
    }
}
